﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DS_21122018
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDemarrer_Click(object sender, EventArgs e)
        {
            string maPhrase = tbPhrase.Text;
            int n = 0;
            char[] tabcharclair = maPhrase.ToCharArray();
            int compteurVoyelles = 0;
            string Voyelles = null;
            while (n < maPhrase.Length)
            {
                string caractString = maPhrase.Substring(n, 1);
                char caractChar = Convert.ToChar(caractString);
                int codeAscii = Convert.ToInt32(caractChar);
                //Console.WriteLine(codeAscii);
                if (codeAscii == 65 || codeAscii == 69 || codeAscii == 73 || codeAscii == 79 || codeAscii == 85 || codeAscii == 89 || codeAscii == 97 || codeAscii == 101 || codeAscii == 105 || codeAscii == 111 || codeAscii == 117 || codeAscii == 121)
                {
                    compteurVoyelles++;
                    char caractChar_ascii = Convert.ToChar(codeAscii);
                    string caractString_ascii = Convert.ToString(caractChar_ascii);
                    Voyelles = Voyelles + ' ' + caractString_ascii;
                    
                }
                n++;
            }
            tbCompteur.Text=Convert.ToString(compteurVoyelles);
            tbVoyelles.Text=Voyelles;
        }
    }
}
