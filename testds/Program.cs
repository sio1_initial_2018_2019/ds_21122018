﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testds
{
    class Program
    {
        static void Main(string[] args)
        {
            string maPhrase = "A la claire fontaine m'en allant promener, j'ai trouvé l'eau si belle que je m'y suis baigné";
            int n = 0;
            char[] tabcharclair = maPhrase.ToCharArray();
            int compteurVoyelles = 0;
            string Voyelles = null;
            while (n < maPhrase.Length)
            {
                string caractString = maPhrase.Substring(n, 1);
                char caractChar = Convert.ToChar(caractString);
                int codeAscii = Convert.ToInt32(caractChar);
                //Console.WriteLine(codeAscii);
                if(codeAscii == 65 || codeAscii == 69 || codeAscii == 73 || codeAscii == 79 || codeAscii == 85 || codeAscii == 89 || codeAscii == 97 || codeAscii == 101 || codeAscii == 105 || codeAscii == 111 || codeAscii == 117 || codeAscii == 121)
                {
                    compteurVoyelles++;
                    char caractChar_ascii = Convert.ToChar(codeAscii);
                    string caractString_ascii = Convert.ToString(caractChar_ascii);
                    Voyelles = Voyelles +' '+ caractString_ascii;
                }
                n++;
            }
            Console.WriteLine(compteurVoyelles);
            Console.WriteLine(Voyelles);
            Console.ReadKey();
        }
    }
}
